package Tugas;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Tugas extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HIEGHT=200;
    private static final int FRAME_X_ORIGIN= 150;
    private static final int FRAME_y_ORIGIN= 250;
    private JLabel labeBil1;
    private JLabel labeBil2;
    private JLabel labeHasil;
    private JTextField text1;
    private JTextField text2;
    private JTextField text3;
    private JButton buttonJumlah;
    private int bil1, bil2, total;
    
    public static void main(String[] args) {
        Tugas frame = new Tugas();
        frame.setVisible(true);
    }
    
    public Tugas(){
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        
        setSize(FRAME_WIDTH, FRAME_HIEGHT);
        setResizable(true);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_y_ORIGIN);
        
        labeBil1 = new JLabel("Panjang (m)");
        labeBil1.setBounds(30, 15, 100, 50);
        contentPane.add(labeBil1);
        
        text1 = new JTextField();
        text1.setBounds(130, 30, 100, 20);
        contentPane.add(text1);
        
        labeBil2 = new JLabel("Lebar (m)");
        labeBil2.setBounds(30, 45, 100, 50);
        contentPane.add(labeBil2);
        
        text2 = new JTextField();
        text2.setBounds(130, 60, 100, 20);
        contentPane.add(text2);
        
        labeHasil = new JLabel("Luas (m2)");
        labeHasil.setBounds(30, 75, 100, 50);
        contentPane.add(labeHasil);
        
        text3 = new JTextField();
        text3.setBounds(130, 90, 100, 20);
        contentPane.add(text3);
        
        buttonJumlah = new JButton("Hitung");
        buttonJumlah.setBounds(130, 120, 80, 20);
        contentPane.add(buttonJumlah);
        
        buttonJumlah.addActionListener(this);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        try{
            int a , b , hasil;
            a=Integer.parseInt(text1.getText());
            b=Integer.parseInt(text2.getText());
            hasil = a*b;
            text3.setText(Integer.toString(hasil));
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,
                    "Maaf , hanya integer yang diperbolehkan",
                    "Eror",JOptionPane.ERROR_MESSAGE);
        }
    }
}